# Todo
Todo app built with TypeScript, React, Node, TypeORM, PostgreSQL.

Deployed project link: https://todo-b7kmxnbcx-jml1996.vercel.app/

Back end base url: https://glacial-plains-63171.herokuapp.com/todos


## To use locally
1. `git clone https://gitlab.com/joshuamlovins/todo.git`
2. `cd todo`
3. `npm i`
4. `npm start`

Voila!

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).