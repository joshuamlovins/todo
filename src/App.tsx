import React, { useState, useEffect } from 'react'
import { v4 as uuid } from 'uuid'
import { TextField } from '@material-ui/core'
import axios from 'axios'

type Todo = { todo: string }

type TodosObj = { id: string, todo: string }

type TodosArr = {
  todos: TodosObj[]
}

const apiBaseUrl = "https://glacial-plains-63171.herokuapp.com/todos"

const App = () => {
  const [formValues, setFormValues] = useState<Todo>({todo: ""})
  const [todos, setTodos] = useState<Array<TodosObj>>([])

  useEffect(() => {
    axios
      .get(apiBaseUrl)
      .then(res => {
        const adjustedForFrontend = res.data.map((todoItem: any) => {
          return {
            id: todoItem.frontEndId,
            todo: todoItem.todo
          }
        })
        setTodos(adjustedForFrontend)
      })
      .catch(err => {
        console.log(err)
      })
  }, [])

  const newListItem = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const id: string = uuid();
    setTodos([...todos, { id: id, todo: formValues.todo }])

    const adjustedForBackend = { 
      frontEndId: id, 
      todo: formValues.todo 
    }

    axios
      .post(apiBaseUrl, adjustedForBackend)
      .then(res => {
        console.log(res)
      })
      .catch(err => {
        console.log(err)
      })

    setFormValues({todo: ""})
  }

  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const target = e.target
    setFormValues({todo: target.value})
  }

  const deleteItem = (itemId: string) => {
    setTodos(todos.filter(v => v.id != itemId))

    const adjustedForBackend = {
        data: { 
        frontEndId: itemId
      }
    }
    
    axios
      .delete(apiBaseUrl, adjustedForBackend)
      .then(res => {
        console.log(res)
      })
      .catch(err => {
        console.log(err)
      })
  }

  const edit = (itemId: string) => {
    let toBeEdited;
    const editedRemoved = todos.filter(v => {
      if (v.id !== itemId) {
        return v
      } else {
        toBeEdited = v
      }
    })
    setTodos(editedRemoved)
    if (toBeEdited) {
      setFormValues(toBeEdited)
    }

    // This (no put; just delete and post) makes frontEndId the source of truth, 
    // as compared to the database primary key id.
    const adjustedForBackend = {
      data: { 
        frontEndId: itemId
      }
    }
    axios
      .delete(apiBaseUrl, adjustedForBackend)
      .then(res => {
        console.log(res)
      })
      .catch(err => {
        console.log(err)
      })
  }

  return (
    <div style={{ marginTop:"80px", textAlign: "center" }}>
      <form onSubmit={newListItem}>
        <TextField
          value={formValues.todo || ""}
          onChange={changeHandler}
          style={{ width: "171px"}}
        />
      </form>
      <ul 
        style={{
          padding: "0", 
          listStyleType: "none", 
          textAlign:"left", 
          width: "171px", 
          margin: "auto", 
          marginTop: "10px"
        }}
      >
        {
          todos.map((item) => 
            <li key={item.id}>
              <span onClick={() => deleteItem(item.id)} style={{ cursor: "pointer"}}>
                -
              </span>
              <span onClick={() => edit(item.id)} key={item.id} style={{ cursor: "pointer"}}>
                {' '} {item.todo}
              </span>
            </li>
          )
        }
      </ul>
    </div>
  );
}

export default App;
